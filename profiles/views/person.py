from rest_framework.views import APIView
from rest_framework.response import Response
from profiles.models import Gender, Person, Ethnicity, Treatment
from django.contrib.auth.models import User
from profiles.serializers import PersonSerializer
from django.db import IntegrityError


class PersonList(APIView):
    def get(self, request):
        people = Person.objects.all()
        serializer = PersonSerializer(people, many=True)
        return Response(serializer.data)

    def post(self, request):
        nome = request.data["name"].split(' ')
        try:
            user = User()
            user.username = request.data["username"]
            user.password = request.data["password"]
            user.email = request.data["email"]
            user.first_name = nome[0]
            user.last_name = nome[len(nome)-1] if len(nome) > 1 else ""
            user.is_staff = False
            user.is_superuser = False
            user.is_active = True
            user.save()
        except IntegrityError:
            return Response({"Error": "Single value already exists or is blank!!!"}, 409)
        try:
            person = Person()
            person.user = user
            person.photo = request.data["photo"]
            person.cellphone = request.data["cellphone"]
            person.birthdate = request.data["birthdate"]
            person.gender = Gender.objects.get(pk=request.data["gender_id"])
            person.ethnicity = Ethnicity.objects.get(pk=request.data["ethnicity"])
            person.treatment = Treatment.objects.get(pk=request.data["treatment"])
            person.save()
        except Gender.DoesNotExist:
            user.delete()
            return Response({"Error": "Gender Not Found"}, 404)
        except Ethnicity.DoesNotExist:
            user.delete()
            return Response({"Error": "Ethnicity Not Found"}, 404)
        except Treatment.DoesNotExist:
            user.delete()
            return Response({"Error": "Treatment Not Found"}, 404)
        except IntegrityError:
            user.delete()
            return Response({"Error": "Single value already exists or is blank!!!"}, 409)
        serializer = PersonSerializer(person, many=False)
        return Response(serializer.data)


class PersonDetail(APIView):
    def get(self, request, id):
        try:
            person = Person.objects.get(pk=id)
        except Person.DoesNotExist:
            return Response({"Error": "Person Not Found"}, 404)
        serializer = PersonSerializer(person, many=False)
        return Response(serializer.data)

    def put(self, request, id):
        nome = request.data["name"].split(' ')
        try:
            person = Person.objects.get(pk=id)
            user = User.objects.get(pk=person.user)
            user.username = request.data["username"]
            user.password = request.data["password"]
            user.email = request.data["email"]
            user.first_name = nome[0]
            user.last_name = nome[len(nome)-1] if len(nome) > 1 else ""
            user.is_staff = request.data["is_staff"]
            user.is_superuser = request.data["is_superuser"]
            user.is_active = request.data["is_active"]
            user.save()
            person.photo = request.data["photo"]
            person.cellphone = request.data["cellphone"]
            person.birthdate = request.data["birthdate"]
            person.gender = Gender.objects.get(pk=request.data["gender_id"])
            person.ethnicity = Ethnicity.objects.get(pk=request.data["ethnicity"])
            person.treatment = Treatment.objects.get(pk=request.data["treatment"])
            person.save()
        except Person.DoesNotExist:
            return Response({"Error": "Person Not Found"}, 404)
        except User.DoesNotExist:
            return Response({"Error": "User Not Found"}, 404)
        except Gender.DoesNotExist:
            return Response({"Error": "Gender Not Found"}, 404)
        except Ethnicity.DoesNotExist:
            return Response({"Error": "Ethnicity Not Found"}, 404)
        except Treatment.DoesNotExist:
            return Response({"Error": "Treatment Not Found"}, 404)
        except IntegrityError:
            return Response({"Error": "Single value already exists or is blank!!!"}, 409)
        serializer = PersonSerializer(person, many=False)
        return Response(serializer.data)

    def delete(self, request, id):
        nome = request.data["name"].split(' ')
        try:
            person = Person.objects.get(pk=id)
            user = User.objects.get(pk=person.user)
            user.is_active = False
            user.save()
        except Person.DoesNotExist:
            return Response({"Error": "Person Not Found"}, 404)
        except User.DoesNotExist:
            return Response({"Error": "User Not Found"}, 404)
        except IntegrityError:
            return Response({"Error": "Single value already exists or is blank!!!"}, 409)
        serializer = PersonSerializer(person, many=False)
        return Response(serializer.data)
