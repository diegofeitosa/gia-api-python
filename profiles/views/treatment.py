from rest_framework.views import APIView
from rest_framework.response import Response
from profiles.models import Treatment
from profiles.serializers import TreatmentSerializer
from django.db import IntegrityError


class TreatmentList(APIView):
    def get(self, request):
        treatments = Treatment.objects.all()
        serializer = TreatmentSerializer(treatments, many=True)
        return Response(serializer.data)

    def post(self, request):
        try:
            treatment = Treatment()
            treatment.name = request.data["name"]
            treatment.abbreviation = request.data["abbreviation"]
            treatment.save()
        except IntegrityError:
            return Response({"Error": "Single value already exists or is blank!!!"}, 409)
        serializer = TreatmentSerializer(treatment, many=False)
        return Response(serializer.data)


class TreatmentDetail(APIView):
    def get(self, request, id):
        try:
            treatment = Treatment.objects.get(pk=id)
        except Treatment.DoesNotExist:
            return Response({"Error": "Pronouns Treatment Not Found"}, 404)
        serializer = TreatmentSerializer(treatment, many=True)
        return Response(serializer.data)

    def put(self, request, id):
        try:
            treatment = Treatment.objects.get(pk=id)
            treatment.name = request.data["name"]
            treatment.abbreviation = request.data["abbreviation"]
            treatment.save()
        except Treatment.DoesNotExist:
            return Response({"Error": "Pronouns Treatment Not Found"}, 404)
        except IntegrityError:
            return Response({"Error": "Single value already exists or is blank!!!"}, 409)
        serializer = TreatmentSerializer(treatment, many=False)
        return Response(serializer.data)

    def delete(self, request, id):
        try:
            treatment = Treatment.objects.get(pk=id)
            treatment.name = request.data["name"]
            treatment.abbreviation = request.data["abbreviation"]
            treatment.save()
        except Treatment.DoesNotExist:
            return Response({"Error": "Pronouns Treatment Not Found"}, 404)
        except IntegrityError:
            return Response({"Error": "Single value already exists or is blank!!!"}, 409)
        serializer = TreatmentSerializer(treatment, many=False)
        return Response(serializer.data)
