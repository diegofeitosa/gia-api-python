from rest_framework.views import APIView
from rest_framework.response import Response
from profiles.models import Gender
from profiles.serializers import GenderSerializer
from django.db import IntegrityError


class GenderList(APIView):
    def get(self, request):
        genders = Gender.objects.all()
        serializer = GenderSerializer(genders, many=True)
        return Response(serializer.data)

    def post(self, request):
        try:
            gender = Gender()
            gender.name = request.data["name"]
            gender.article = request.data["article"]
            gender.pronouns = request.data["pronouns"]
            gender.save()
        except IntegrityError:
            return Response({"Error": "Single value already exists or is blank!!!"}, 409)
        serializer = GenderSerializer(gender, many=False)
        return Response(serializer.data, 201)


class GenderDetail(APIView):
    def get(self, request, id):
        try:
            gender = Gender.objects.get(pk=id)
        except Gender.DoesNotExist:
            return Response({"Error": "Gender Not Found"}, 404)
        serializer = GenderSerializer(gender, many=False)
        return Response(serializer.data)

    def put(self, request, id):
        try:
            gender = Gender.objects.get(pk=id)
            gender.name = request.data["name"]
            gender.article = request.data["article"]
            gender.pronouns = request.data["pronouns"]
            gender.save()
        except Gender.DoesNotExist:
            return Response({"Error": "Gender Not Found"}, 404)
        except IntegrityError:
            return Response({"Error": "Single value already exists or is blank!!!"}, 409)
        serializer = GenderSerializer(gender, many=False)
        return Response(serializer.data)

    def delete(self, request, id):
        try:
            gender = Gender.objects.get(pk=id)
            gender.delete()
        except Gender.DoesNotExist:
            return Response({"Error": "Ethnicity Not Found"}, 404)
        serializer = GenderSerializer(gender, many=False)
        return Response(serializer.data, 204)
