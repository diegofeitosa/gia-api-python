#!/usr/bin/python
# -*- coding: utf-8 -*-
from rest_framework.views import APIView
from rest_framework.response import Response
from profiles.models import Ethnicity
from profiles.serializers import EthnicitySerializer
from django.db import IntegrityError


class EthnicityList(APIView):
    def get(self, request):
        ethnicities = Ethnicity.objects.all()
        serializer = EthnicitySerializer(ethnicities, many=True)
        return Response(serializer.data)

    def post(self, request):
        try:
            ethnicity = Ethnicity()
            ethnicity.name = request.data["name"] if request.data["name"] else None
            ethnicity.parent = Ethnicity.objects.get(pk=request.data["parent"]) if request.data["parent"] else None
            ethnicity.save()
        except IntegrityError:
            return Response({"Error": "Single value already exists or is blank!!!"}, 409)
        except KeyError as e:
            return Response({"Error": "The request parameter '" + str(e.args[0]) + "' was not found"}, 409)
        serializer = EthnicitySerializer(ethnicity, many=False)
        return Response(serializer.data, 201)


class EthnicityDetail(APIView):
    def get(self, request, id):
        try:
            ethnicity = Ethnicity.objects.get(pk=id)
        except Ethnicity.DoesNotExist:
            return Response({"Error": "Ethnicity Not Found"}, 404)
        serializer = EthnicitySerializer(ethnicity, many=False)
        return Response(serializer.data)

    def put(self, request, id):
        try:
            ethnicity = Ethnicity.objects.get(pk=id)
            ethnicity.name = request.data["name"]
            ethnicity.parent = request.data["parent"]
            ethnicity.save()
        except Ethnicity.DoesNotExist:
            return Response({"Error": "Ethnicity Not Found"}, 404)
        except IntegrityError:
            return Response({"Error": "Single value already exists or is blank!!!"}, 409)
        serializer = EthnicitySerializer(ethnicity, many=False)
        return Response(serializer.data)

    def delete(self, request, id):
        try:
            ethnicity = Ethnicity.objects.get(pk=id)
            ethnicity.delete()
        except Ethnicity.DoesNotExist:
            return Response({"Error": "Ethnicity Not Found"}, 404)
        serializer = EthnicitySerializer(ethnicity, many=False)
        return Response(serializer.data, 204)
