from django.urls import path
from profiles.views import gender, ethnicity, treatment, person

urlpatterns = [
    path('', person.PersonList.as_view()),
    path('<uuid:id>/', person.PersonDetail.as_view()),
    path('genders/', gender.GenderList.as_view()),
    path('genders/<int:id>/', gender.GenderDetail.as_view()),
    path('ethnicities/', ethnicity.EthnicityList.as_view()),
    path('ethnicities/<int:id>/', ethnicity.EthnicityDetail.as_view()),
    path('treatments/', treatment.TreatmentList.as_view()),
    path('treatments/<int:id>/', treatment.TreatmentDetail.as_view()),
]