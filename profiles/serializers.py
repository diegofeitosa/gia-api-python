# -*- coding: utf-8 -*-
from rest_framework import serializers
from .models import Gender, Ethnicity, Treatment, Person


class GenderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Gender
        fields = ['name', 'slug', 'article', 'pronouns']


class EthnicitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Ethnicity
        fields = ['name', 'slug', 'parent']


class TreatmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Treatment
        fields = ['name', 'slug', 'abbreviation']


class PersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = ['id', 'name', 'photo', 'birthdate', 'cellphone', 'gender', 'ethnicity', 'treatment']
