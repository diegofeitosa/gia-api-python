#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
import uuid
from datetime import date
from django.utils.text import slugify


class Gender(models.Model):
    name = models.CharField(max_length=32, unique=True)
    slug = models.SlugField(max_length=32, unique=True)
    article = models.CharField(max_length=3)
    pronouns = models.CharField(max_length=6)

    def __str__(self):
        return self.name + " ('" + self.article + "', '" + self.pronouns + "' )"

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)


class Ethnicity(models.Model):
    name = models.CharField(max_length=32, unique=True)
    slug = models.SlugField(max_length=32, unique=True)
    parent = models.ForeignKey("self", null=True, blank=True, on_delete=models.RESTRICT)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)


class Treatment(models.Model):
    name = models.CharField(max_length=32, unique=True)
    slug = models.SlugField(max_length=32, unique=True)
    abbreviation = models.CharField(max_length=6)

    def __str__(self):
        return self.name + " ( " + self.abbreviation + " )"
    
    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)


class Person(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.OneToOneField(User, related_name='person', on_delete=models.CASCADE)
    photo = models.ImageField(upload_to='photos', null=True, blank=True)
    cellphone = models.CharField(max_length=16, null=True, blank=True)
    birthdate = models.DateField(null=True, blank=True)
    gender = models.OneToOneField(Gender, null=True, blank=True, on_delete=models.RESTRICT)
    ethnicity = models.OneToOneField(Ethnicity, null=True, blank=True, on_delete=models.RESTRICT)
    treatment = models.OneToOneField(Treatment, null=True, blank=True, on_delete=models.RESTRICT)

    def __str__(self):
        return self.user.first_name + " " + self.user.last_name

    @property
    def name(self):
        return self.user.first_name + " " + self.user.last_name

    @property
    def old(self):
        return date(self.birthdate) - date.today()

    def __str__(self):
        return self.user.first_name + " " + self.user.last_name
